import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  form!: FormGroup;
  name: string[] = []
  datos!: string;

  constructor(private fb: FormBuilder) { 
    this.crearForm();
  }

  
  get pasatiemposO() {
    return this.form.get('pasatiempos') as FormArray;
    }

  ngOnInit(): void {
  }  
  
  crearForm(): void{
    this.form = this.fb.group({
      nombre: [''],
      pasatiempos: this.fb.array([])
    })
  }

  adicionar(): void{
    console.log('ADICIONAR');
    console.log(this.form.value.nombre);
    
    this.name.push(this.form.value.nombre)

    this.pasatiemposO.push(this.fb.control(''))

    this.form.value.nombre = ''
   }

   borrarCaja(i: number): void{
    this.pasatiemposO.removeAt(i);
  }

  vaciarcaja():void {
    console.log(this.datos);
    
    this.datos = ''
  }

  guardar(): void{
    console.log('guardado');
    this.datos = 'Id:' + this.name + ' - Nombre:' + this.name
  }
}
